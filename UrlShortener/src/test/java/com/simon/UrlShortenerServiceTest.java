package com.simon;

import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


import com.simon.models.Models;
import com.simon.shortener.UrlRepository;
import com.simon.shortener.UrlServiceImpl;
import com.simon.shortener.UrlShort;




@RunWith(MockitoJUnitRunner.class)
public class UrlShortenerServiceTest {

	private static final String TINY_URL_KEY = "f8dE4ZUIjd";
	
	@Mock
	private UrlRepository urlRepository;

	@InjectMocks
	private UrlServiceImpl urlService;

	@Before
	public void setup() {
		when(urlRepository.findByTinyUrl(TINY_URL_KEY)).thenReturn(Optional.of(Models.createUrlShort()));
		when(urlRepository.save(Mockito.any(UrlShort.class))).thenReturn(Models.createUrlShort());
	}
	
	@Test
	public void findByTinyUrlTest() {
		final Optional<UrlShort> urlShort = urlService.findByTinyUrl(TINY_URL_KEY);
		Assert.assertNotNull(urlShort.get());
		Assert.assertTrue(Models.PLAIN_URL.equals(urlShort.get().getPlainUrl()));

		Mockito.verify(urlRepository, Mockito.times(1)).findByTinyUrl(Mockito.anyString());
	}

	@Test
	public void saveTest() throws Exception {
		urlService.save(Models.createUrlShort());

		Mockito.verify(urlRepository, Mockito.times(1)).save(Mockito.any(UrlShort.class));
	}
	
}
