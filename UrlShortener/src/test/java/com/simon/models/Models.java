package com.simon.models;

import com.simon.shortener.UrlShort;

public class Models {
	
public static final String TINY_URL_KEY = "f8dE4ZUIjd";
public static final String PLAIN_URL = "www.test.com";
public static final String PLAIN_URL_PARAM = "http://www.google.com";
public static final String PLAIN_URL_PARAM_CLEAN = "www.google.com";
	
	/**
	 * Create a UrlShort.
	 * 
	 * @return urlShort.
	 */
	public static UrlShort createUrlShort() {
		final UrlShort urlShort = new UrlShort();
		urlShort.setTinyUrl(TINY_URL_KEY);
		urlShort.setPlainUrl(PLAIN_URL);
		return urlShort;
	}
	
	public static UrlShort createUrlShortPlainUrl() {
		final UrlShort urlShort = new UrlShort();
		urlShort.setPlainUrl(PLAIN_URL_PARAM);
		return urlShort;
	}

}
