package com.simon;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.simon.shortener.UrlRepository;
import com.simon.shortener.UrlShort;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UrlRepositoryTests {
	
	private static final String TINY_URL_KEY_1 = "f8dE4ZUIjd";
	private static final String PLAIN_URL_1 = "https://www.google.com/";
	private static final String TINY_URL_KEY_2 = "k9hy67ETRj";
	private static final String PLAIN_URL_2 = "https://www.test.com/";
	
	@Autowired
	private UrlRepository repository;
	
	@Before
	public void setup() throws Exception {
        UrlShort url1= new UrlShort(TINY_URL_KEY_1, PLAIN_URL_1);
        UrlShort url2= new UrlShort(TINY_URL_KEY_2, PLAIN_URL_2);
        assertNull(url1.getId());
        assertNull(url2.getId());
        this.repository.save(url1);
        this.repository.save(url2);
        assertNotNull(url1.getId());
        assertNotNull(url2.getId());
    }
	
	@Test
	public void findByTinyUrlTest() throws Exception {
		Optional<UrlShort> urlShortDb = repository.findByTinyUrl(TINY_URL_KEY_1);
		assertTrue(urlShortDb.isPresent());
		assertThat(urlShortDb.get().getPlainUrl()).isEqualTo(PLAIN_URL_1);
	}
	
	

}
