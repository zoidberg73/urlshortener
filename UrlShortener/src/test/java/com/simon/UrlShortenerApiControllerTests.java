package com.simon;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.simon.models.Models;
import com.simon.shortener.UrlControllerApi;
import com.simon.shortener.UrlService;
import com.simon.shortener.UrlShort;
import com.simon.shortener.UrlShortener;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@RunWith(SpringRunner.class)
@WebMvcTest(UrlControllerApi.class)
public class UrlShortenerApiControllerTests {
	
	private static final String REQUEST_PATH = "/";
	private static final String TINY_EXISTS = "f8dE4ZUIxy";
	private static final String TINY_NOT_EXISTS = "xyxyxyxyxy";
	private static final String REQUEST_PATH_WITH_TINY = REQUEST_PATH + TINY_EXISTS;
	private static final String REQUEST_PATH_WITH_TINY_NOT_EXISTS = REQUEST_PATH + TINY_NOT_EXISTS;
	
	private Gson gson;
	
	@Autowired
	private MockMvc mvc;

	@MockBean
	private UrlService urlShortenerServiceMock;
	
	@MockBean
	private UrlShortener urlShortenerMock;

	@Before
	public void setup() throws Exception {
		gson = new GsonBuilder().create();
		when(urlShortenerMock.generateTinyUrl(Models.PLAIN_URL_PARAM_CLEAN)).thenReturn(TINY_NOT_EXISTS);
		when(urlShortenerServiceMock.findByTinyUrl(TINY_EXISTS)).thenReturn(Optional.of(new UrlShort()));
		when(urlShortenerServiceMock.findByTinyUrl(TINY_NOT_EXISTS)).thenReturn(Optional.empty());
		when(urlShortenerServiceMock.findByPlainUrl(Models.PLAIN_URL_PARAM_CLEAN)).thenReturn(Optional.empty());
		when(urlShortenerServiceMock.save(Mockito.mock(UrlShort.class))).thenReturn(new UrlShort());
	}
	
	@Test
	public void findUrlShortByTinyUrlTest() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get(REQUEST_PATH_WITH_TINY).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test
	public void findUrlShortByTinyUrlNotExistTest() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get(REQUEST_PATH_WITH_TINY_NOT_EXISTS).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test
	public void saveNewUrlShortTest() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post(REQUEST_PATH).accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(gson.toJson(Models.createUrlShortPlainUrl())))
				.andExpect(status().isCreated());
	}


}
