package com.simon.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

	
	public static class UrlValidator{
		
		private static final String URL_REGEX = 
				"^((((https?|ftps?|gopher|telnet|nntp)://)|(mailto:|news:))"
				+ "(%[0-9A-Fa-f]{2}|[-()_.!~*';/?:@&=+$,A-Za-z0-9])+)"
				+ "([).!';/?:,][[:blank:]])?$";
		
		private static final Pattern URL_PATTERN = Pattern.compile(URL_REGEX);
		
		public static boolean validateUrl(String url){
			if(url==null) return false;
			Matcher matcher = URL_PATTERN.matcher(url);
			return matcher.matches();
		}
	}
	
	public static String cleanURL(String url) {
		if (url.substring(0, 7).equals("http://"))
			url = url.substring(7);

		if (url.substring(0, 8).equals("https://"))
			url = url.substring(8);

		if (url.charAt(url.length() - 1) == '/')
			url = url.substring(0, url.length() - 1);
		return url;
	}
	
	public static <E> List<E> toList(Iterable<E> iterable) {
		if (iterable instanceof List) {
			return (List<E>) iterable;
		}
		ArrayList<E> list = new ArrayList<E>();
		if (iterable != null) {
			for (E e : iterable) {
				list.add(e);
			}
		}
		return list;
	}
	
	
}
