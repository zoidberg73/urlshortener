package com.simon.exceptions;

public class ContentNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ContentNotFoundException () {
		super ("No content found");
    }

    public ContentNotFoundException (String message) {
        super (message);
    }
}
