package com.simon.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.simon.errors.ApiError;

@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler({BadUrlParameterException.class})
    protected ResponseEntity<ApiError> handleBadParameter(RuntimeException ex, WebRequest request) {
		return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage()));
    }
	
	@ExceptionHandler({ContentNotFoundException.class})
    protected ResponseEntity<ApiError> handleNoContentFound(RuntimeException ex, WebRequest request){
		return buildResponseEntity(new ApiError(HttpStatus.OK, ex.getMessage()));
    }
	private ResponseEntity<ApiError> buildResponseEntity(ApiError apiError) {
		return new ResponseEntity<>(apiError,apiError.getStatus());
	}
	
	
}
