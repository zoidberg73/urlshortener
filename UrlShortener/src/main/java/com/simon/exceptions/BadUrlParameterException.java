package com.simon.exceptions;

public class BadUrlParameterException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public BadUrlParameterException () {
		super ("This is not a valid Url");
    }

    public BadUrlParameterException (String message) {
        super (message);
    }
}
