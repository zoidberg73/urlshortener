package com.simon.shortener;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface UrlRepository extends CrudRepository<UrlShort, String> {

	Optional<UrlShort> findByTinyUrl(String tinyUrl);
	
	Optional<UrlShort> findByPlainUrl(String plainUrl);
}
