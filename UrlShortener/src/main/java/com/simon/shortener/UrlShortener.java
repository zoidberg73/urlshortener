package com.simon.shortener;

import java.util.Random;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UrlShortener {
	
	private static UrlShortener instance = new UrlShortener();
	
	private char myChars[]; 
	
	@Value("${tinyUrl.length}")
	private int tinyLength = 10;
	
	
	private UrlShortener() {
		myChars = new char[62];
		for (int i = 0; i < 62; i++) {
			int j = 0;
			if (i < 10) {
				j = i + 48;
			} else if (i > 9 && i <= 35) {
				j = i + 55;
			} else {
				j = i + 61;
			}
			myChars[i] = (char) j;
		}
	}
	
   public static UrlShortener getInstance(){
      return instance;
   }		
	
	public String generateTinyUrl(String plainUrl) {
		String tinyUrl = "";
		Random rand = new Random();
		for (int i = 0; i <= this.tinyLength-1; i++) {
			tinyUrl += myChars[rand.nextInt(62)];
		}
		return tinyUrl;
	}
	
}
