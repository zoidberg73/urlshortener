package com.simon.shortener;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.simon.exceptions.BadUrlParameterException;
import com.simon.exceptions.ContentNotFoundException;
import com.simon.utils.Utils;

@CrossOrigin
@RestController
public class UrlControllerApiImpl implements UrlControllerApi{
	
	@Autowired
	UrlService urlService;
	@Autowired
	UrlShortener urlShortener;
	@Autowired
	private Environment env;
	
	@Override
	public ResponseEntity<UrlShort> getPlainUrl(@PathVariable String tinyUrl) {
        //Return Plain Url
		final Optional<UrlShort> urlShort = urlService.findByTinyUrl(tinyUrl);
		if(!urlShort.isPresent()){
			throw new ContentNotFoundException();
		} 
		HttpHeaders responseHeaders = new HttpHeaders();
        return new ResponseEntity<>(urlShort.get(), responseHeaders, HttpStatus.OK);
    }
	
	@Override
	public ResponseEntity<UrlShort> shortenUrl(@RequestBody final UrlShort urlShort,BindingResult result) {
		if(urlShort==null) throw new BadUrlParameterException();
		//Check if the given url is valid
		if(!Utils.UrlValidator.validateUrl(urlShort.getPlainUrl())) throw new BadUrlParameterException();
		//Clean the Url
		urlShort.setPlainUrl(Utils.cleanURL(urlShort.getPlainUrl()));
		//Check if Url Already exists in DB
		Optional<UrlShort> toCheck = urlService.findByPlainUrl(urlShort.getPlainUrl());
		//if already exists return the urlShort
		if(toCheck.isPresent()){
			return new ResponseEntity<UrlShort>(toCheck.get(), HttpStatus.OK);
		}
		//Calculate the tiny url
		String newTiny = urlShortener.generateTinyUrl(urlShort.getPlainUrl());
		while(urlService.findByTinyUrl(newTiny).isPresent()){
			newTiny = urlShortener.generateTinyUrl(urlShort.getPlainUrl());
		}
		urlShort.setTinyUrl(newTiny);
		// Guarantees it is a creation, not an update
		urlShort.setId(null);
		/* Save in db. */
		try {
			UrlShort shortened = urlService.save(urlShort);
			//Then add the domain to tinyUrl before returning the response
			if(shortened != null && env.getProperty("url.shortener.domain") != null){
				shortened.setTinyUrl(env.getProperty("url.shortener.domain")+urlShort.getTinyUrl());
			}
			return new ResponseEntity<UrlShort>(shortened, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<UrlShort>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}
	
	@Override
	public ResponseEntity<List<UrlShort>> findAll() {
		final List<UrlShort> templates = urlService.findAll();
		if (templates.isEmpty()) {
			throw new ContentNotFoundException();
		}
		return new ResponseEntity<List<UrlShort>>(templates, HttpStatus.OK);
	}


}
