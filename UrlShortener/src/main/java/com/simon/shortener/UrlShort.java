package com.simon.shortener;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Document
public class UrlShort {

	@Id
	@JsonIgnore
	private String id;
	@Indexed
	private String tinyUrl;
	
	private String plainUrl;

	public UrlShort() {
		super();
	}
	
	public UrlShort(String tinyUrl, String plainUrl) {
		super();
		this.tinyUrl = tinyUrl;
		this.plainUrl = plainUrl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTinyUrl() {
		return tinyUrl;
	}

	public void setTinyUrl(String tinyUrl) {
		this.tinyUrl = tinyUrl;
	}

	public String getPlainUrl() {
		return plainUrl;
	}

	public void setPlainUrl(String plainUrl) {
		this.plainUrl = plainUrl;
	}

	@Override
	public String toString() {
		return "Url [id=" + id + ", tinyUrl=" + tinyUrl + ", plainUrl=" + plainUrl + "]";
	}
	
	
	
}
