package com.simon.shortener;

import java.util.List;
import java.util.Optional;

public interface UrlService {

	/**
	 * Save a urlShort.
	 *
	 * @param urlshort
	 *            urlshort to create.
	 * @return created urlshort.
	 * @throws Exception
	 */
	UrlShort save(UrlShort urlShort) throws Exception;
	
	/**
	 * Find UrlShort by tinyUrl.
	 *
	 * @param tinyUrl
	 *            string.
	 * @return a UrlShort.
	 */
	Optional<UrlShort> findByTinyUrl(String tinyUrl);
	
	/**
	 * Find UrlShort by plainUrl.
	 *
	 * @param plainUrl
	 *            string.
	 * @return a UrlShort.
	 */
	Optional<UrlShort> findByPlainUrl(String plainUrl);
	
	/**
	 * Get all the urlshorts.
	 * 
	 * @return a list of urlshorts.
	 */
	List<UrlShort> findAll();
}
