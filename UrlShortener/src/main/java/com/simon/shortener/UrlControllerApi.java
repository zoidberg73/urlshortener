package com.simon.shortener;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface UrlControllerApi {
	
	@RequestMapping(value = "/{tinyUrl}", produces = { "application/json" }, method = RequestMethod.GET)
	ResponseEntity<UrlShort> getPlainUrl(@PathVariable("tinyUrl") String tinyUrl);
	
	@RequestMapping(value = "", produces = { "application/json" }, consumes = {
	"application/json" }, method = RequestMethod.POST)
	ResponseEntity<UrlShort> shortenUrl(@RequestBody UrlShort plainUrl,BindingResult result);
	
	@RequestMapping(value = "/all", produces = { "application/json" }, method = RequestMethod.GET)
	ResponseEntity<List<UrlShort>> findAll();

}
