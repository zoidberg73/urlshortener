package com.simon.shortener;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simon.utils.Utils;

@Service
public class UrlServiceImpl implements UrlService {

	@Autowired
	UrlRepository urlRepository;
	
	@Override
	public UrlShort save(final UrlShort urlShort) throws Exception {
		UrlShort savedUrl = null;
		try {
			savedUrl = urlRepository.save(urlShort);
		} catch (Exception ex) {
			System.err.println("Error while saving url "+urlShort+" "+ex.getMessage());
		}
		return savedUrl;
	}
	
	@Override
	public Optional<UrlShort> findByTinyUrl(String tinyUrl) {
		return urlRepository.findByTinyUrl(tinyUrl);
	}
	
	@Override
	public Optional<UrlShort> findByPlainUrl(String plainUrl) {
		return urlRepository.findByPlainUrl(plainUrl);
	}
	
	@Override
	public List<UrlShort> findAll() {
		return Utils.toList(urlRepository.findAll());
	}
	
	

}
