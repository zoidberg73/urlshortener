# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Running the UrlShortener ###

- Of course get all sources from repo
- Run a mongo instance
- Go to the project root folder
- In the command line :
	- mvn clean package
	- mvn spring-boot:run


- The app should be up and running

- Generate your first tiny urls by opening UrlShortener.html (at the root of the project) in your browser. Copy/Paste an url to shorten and click "Generate"
- Get all generated tiny urls with http://localhost:8080/all
- Get one tiny url with http://localhost:8080/{tinyUrlParameter}

*Hopefully that should work :)